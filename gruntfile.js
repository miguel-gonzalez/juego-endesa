'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jade: {  
        develop: {  
          options:{  
            pretty: true 
          },  
           files: [{

          cwd: 'jade',

          src: '*.jade',

          expand: true,

          ext: '.html'

        }]
        },  
        production: {  
          options:{  
            pretty: false  
          },  
            
          files:[{ expand: true, src: "*.jade", dest: "<%= pkg.name %>-<%= new Date().getDate() %>-<%= new Date().getMonth() %>-<%= new Date().getFullYear() %>/", ext: ".html", cwd: "jade" }]  
        }
    },  
    sass: {
        dist: {
          files: {
            'css/styles.css' : 'scss/*.scss'
          }
        }
    },
    
        concat: {
            dist: {
                src: ['css/bootstrap.min.css','css/styles.css'],
                dest: 'tmp/prod.css',
            }

        },
        cssmin: {
            build: {
                src: 'tmp/prod.css',
                dest: 'css/styles.min.css',
            }
        },

        
        uglify: {
            multifile: {
                files: {
                    'js/scripts.min.js': ['js/jquery.min.js','js/bootstrap.min.js','js/main.js'],
                    'js/scripts-ie8.min.js': ['js/jquery.min.js','js/bootstrap.min.js', 'js/html5shiv.min.js', 'js/respond.min.js', 'js/main.js'],
                },
                options: {
                    mangle: false
                }
            },
        },
        
        clean: {
            temporary: ["tmp/"],
            prod: {
              src: ["<%= pkg.name %>/fonts/**/*.svg","<%= pkg.name %>/css/*.css", "!<%= pkg.name %>/css/styles.css", "!<%= pkg.name %>/css/styles.min.css", "<%= pkg.name %>/js/*.js", "!<%= pkg.name %>/js/scripts.min.js", "!<%= pkg.name %>/js/scripts-ie8.min.js"]
            }
        },

        watch: {
            css: {
              files: 'scss/*.scss',
              tasks: ['sass']
            },
            styles: {
              files: 'css/styles.css',
              tasks: ['concat', 'cssmin', 'clean:temporary']
            },
            js: {
              files: 'js/main.js',
              tasks: ['uglify']
            },
            jade: {
              files: 'jade/**/*.jade',
              tasks: ['jade:develop']
            }

        },

    remove: {
        options: {
          trace: true
      },
        dirList: ['<%= pkg.name %>']
    },
     

    copy: {
      main: {
        files: [
          // includes files within path
          {expand: true, src: ['js/*', 'css/*', 'img/*', 'fonts/**','*.html','!*.svg' ], dest: '<%= pkg.name %>/', filter: 'isFile'},

        
        ]
      }
    }
    
  });
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-remove');

  // Where we tell Grunt what to do when we type "grunt" into the terminal.
  grunt.registerTask('default',['jade:develop','sass', 'concat','cssmin','uglify','clean:temporary','watch']); 

  grunt.registerTask('prod',['jade:production']); 
  // Where we tell Grunt what to do when we type "grunt final" into the terminal.
  grunt.registerTask('final', ['remove','copy','concat','uglify', 'cssmin', 'clean']); 
}